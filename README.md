# ci-docker-image

GitLab CI has some quirks. One of them is the secret files management.
It requires certain ruby scripts to be executed and leads to CI scripts like the one below:

```
ssh deploy:
  image: ruby:latest
  stage: deploy
  variables:
    SSH_PORT: 22
    SECURE_FILES_DOWNLOAD_PATH: './keys/'
  script:
    - apt-get update && apt-get install -y bash ca-certificates curl gnupg lsb-release openssh-client rsync
    - mkdir -p /etc/apt/keyrings
    - curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    - echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list
    - apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/load-secure-files/-/raw/main/installer" | bash
    - chmod 600 $SECURE_FILES_DOWNLOAD_PATH/id_* && eval $(ssh-agent -s) && ssh-add $SECURE_FILES_DOWNLOAD_PATH/id_*
    - ssh -o StrictHostKeyChecking=no -p "$SSH_PORT" "$SERVER_USER@$SERVER_ADDR" "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - ssh -o StrictHostKeyChecking=no -p "$SSH_PORT" "$SERVER_USER@$SERVER_ADDR" "docker pull $CI_REGISTRY_IMAGE:latest"
    - DOCKER_HOST="ssh://$SERVER_USER@$SERVER_ADDR:$SSH_PORT" docker compose up -d
    - echo "✅ deployed"
```

This docker image prebuilds `docker`, `docker compose` inside `ruby:latest` so that you don't have to.

## Usage

```
ssh deploy:
  image: registry.gitlab.com/banderovets/ci-docker-image:latest
  ...
```
